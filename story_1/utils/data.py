from datetime import date
from .funcs import calculate_age

data = {
  'name': 'Hanif Arkan Audah',
  'age': calculate_age(date(2002, 7, 31).year),
  'npm': '1906293070',
  'angkatan': '2019',
  'hobi': 'Saya memiliki hobi untuk bersepeda saat pagi hari. Selain itu saya juga gemar berenang, tetapi sekarang saya jarang melakukan kegiatan tersebut karena rasa pandemi dan rasa malas.',
  'deskripsi': 'Saya merupakan mahasiswa Universitas Indonesia yang sedang mengambil jurusan ilmu komputer di Fakultas Ilmu Komputer. Sebelum COVID-19 saya tinggal di indekos yang berada di daerah kukusan teknik, tetapi sekarang saya tinggal di rumah bersama keluarga saya.',
  'tempat_kuliah': 'Fakultas Ilmu Komputer, Universitas Indonesia',
  'foto': 'story_1/personal_pic.jpg',
  'riwayat_pendidikan': [
    {
      'jenjang': 'Sekolah Dasar',
      'tahun': '2008-2009',
      'nama_sekolah': 'SDN Batan Indah'
    },
    {
      'jenjang': 'Sekolah Dasar',
      'tahun': '2009-2011',
      'nama_sekolah': 'Middle East International School'
    },
    {
      'jenjang': 'Sekolah Dasar',
      'tahun': '2011-2011',
      'nama_sekolah': 'Sekolah Indonesia Riyadh'
    },
    {
      'jenjang': 'Sekolah Dasar',
      'tahun': '2011-2012',
      'nama_sekolah': 'Kran International School'
    },
    {
      'jenjang': 'Sekolah Dasar',
      'tahun': '2012-2013',
      'nama_sekolah': 'SDN Puspiptek'
    },
    {
      'jenjang': 'Sekolah Menegah Pertama',
      'tahun': '2013-2016',
      'nama_sekolah': 'SMP Negeri 8 Tangerang Selatan'
    },
    {
      'jenjang': 'Sekolah Menengah Atas',
      'tahun': '2016-2019',
      'nama_sekolah': 'SMA Negeri 34 Jakarta'
    }
  ],
  'media_sosial': [
    {
      'nama_platform': 'Line',
      'id': 'audahaudah'
    },
    {
      'nama_platform': 'Instagram',
      'id': '@aud.ah'
    },
    {
      'nama_platform': 'Gmail',
      'id': 'hanif.arkan@ui.ac.id'
    }
  ]
}